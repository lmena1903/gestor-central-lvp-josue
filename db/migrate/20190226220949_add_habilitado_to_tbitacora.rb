class AddHabilitadoToTbitacora < ActiveRecord::Migration[5.0]
  def change
    add_column :tbitacora, :habilitado, :boolean
  end
end
