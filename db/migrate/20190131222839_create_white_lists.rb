class CreateWhiteLists < ActiveRecord::Migration[5.0]
  def change
    create_table :white_lists do |t|
      t.integer :IdUsuario
      t.varchar :IdTran
      t.string :Fecha
      t.boolean :Estado

      t.timestamps
    end
  end
end
