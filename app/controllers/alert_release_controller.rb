class AlertReleaseController < ApplicationController
  @@byAlertRelease = "Alert Release"

  def index
    if current_user

      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Alert Release' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearLiberacion = permisos.crear
          @editarLiberacion = permisos.editar
          @leerLiberacion = permisos.leer
          @eliminarLiberacion = permisos.eliminar

          if permisos.view_name == @@byAlertRelease

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@editar == 4) || (@@leer == 2))
            @atendiendo = Tatendiendo.all.page(params[:page]).per(20)

            if params[:release].present?
              @alerta_libera = params[:id_alert]
              @alerta_libera = Tatendiendo.find(@alerta_libera)

              if @alerta_libera.present?
                @alerta_libera.destroy
              end

              @atendiendo = Tatendiendo.all.page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end
end
