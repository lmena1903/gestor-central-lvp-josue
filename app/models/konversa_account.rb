class KonversaAccount
  @@adress = ''

  def initialize

  end

  def self.setUri(uri)
    if uri.to_s.end_with? "/"
      @@adress = uri.to_s
    else
      @@adress = "#{uri.to_s}" + "/"
    end
  end

  def blockAccount(account)
    begin
      response = RestClient.post(@@adress + account.to_s, "data=my", 'Content-Type' => 'application/json')
    rescue Exception => e
      puts "Problema al consumir el web service: " + e.message.to_s

    end
  end

  def unblockAccount(account)
    begin
      response = RestClient.delete(@@adress + account.to_s, 'Content-Type' => 'application/json')
    rescue Exception => e
      puts "Problema al consumir el web service: " + e.message.to_s
      return "Problema al consumir el web service: " + e.message.to_s
    end
    return true
  end
end