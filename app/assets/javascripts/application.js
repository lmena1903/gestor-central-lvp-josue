// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require peity/jquery.peity.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js

// IMPORTANT: APP CONFIG
//= require app.config

// JS TOUCH : include this plugin for mobile drag / drop touch events
//= require plugin/jquery-touch/jquery.ui.touch-punch

// BOOTSTRAP JS

// CUSTOM NOTIFICATION
//= require notification/SmartNotification

// JARVIS WIDGETS
//= require smartwidgets/jarvis.widget

// EASY PIE CHARTS
//= require plugin/easy-pie-chart/jquery.easy-pie-chart

// SPARKLINES
//= require plugin/sparkline/jquery.sparkline

// JQUERY VALIDATE
//= require plugin/jquery-validate/jquery.validate

// JQUERY MASKED INPUT
//= require plugin/masked-input/jquery.maskedinput

// JQUERY SELECT2 INPUT
//= require plugin/select2/select2

// JQUERY UI + Bootstrap Slider
//= require plugin/bootstrap-slider/bootstrap-slider

// browser msie issue fix
//= require plugin/msie-fix/jquery.mb.browser

// FastClick: For mobile devices
//= require plugin/fastclick/fastclick

// MAIN APP JS FILE
//= require app

// ENHANCEMENT PLUGINS : NOT A REQUIREMENT
// Voice command : plugin
//= require speech/voicecommand



//= require plugin/bootstrap-progressbar/bootstrap-progressbar
//= require plugin/jquery-nestable/jquery.nestable

//= require plugin/datatables/jquery.dataTables
//= require plugin/datatables/dataTables.colVis
//= require plugin/datatables/dataTables.tableTools
//= require plugin/datatables/dataTables.bootstrap
//= require plugin/datatable-responsive/datatables.responsive

//= require plugin/jqgrid/jquery.jqGrid
//= require plugin/jqgrid/grid.locale-en

//= require plugin/highChartCore/highcharts-custom
//= require plugin/highchartTable/jquery.highchartTable

//= require plugin/maxlength/bootstrap-maxlength
//= require plugin/bootstrap-timepicker/bootstrap-timepicker
//= require plugin/clockpicker/clockpicker
//= require plugin/bootstrap-tags/bootstrap-tagsinput
//= require plugin/noUiSlider/jquery.nouislider
//= require plugin/ion-slider/ion.rangeSlider
//= require plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox
//= require plugin/colorpicker/bootstrap-colorpicker
//= require plugin/knob/jquery.knob
//= require plugin/x-editable/moment.min
//= require plugin/x-editable/jquery.mockjax.min
//= require plugin/x-editable/x-editable
//= require plugin/typeahead/typeahead
//= require plugin/typeahead/typeaheadjs
//= require plugin/bootstrapvalidator/bootstrapValidator
//= require plugin/bootstrap-wizard/jquery.bootstrap.wizard
//= require plugin/fuelux/wizard/wizard
//= require plugin/dropzone/dropzone
//= require plugin/jquery-form/jquery-form

//= require plugin/summernote/summernote
//= require plugin/markdown/markdown
//= require plugin/markdown/to-markdown
//= require plugin/markdown/bootstrap-markdown
//= require plugin/superbox/superbox



// Morris Chart Dependencies
//= require plugin/morris/morris
//= require plugin/morris/raphael
//= require plugin/morris/morris-chart-settings



// DYGRAPH
//= require plugin/dygraphs/dygraph-combined

//= require plugin/dygraphs/demo-data

//= require plugin/delete-table-row/delete-table-row
//= require plugin/summernote/summernote

// PAGE RELATED PLUGIN(S)

// Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip
// = require plugin/flot/jquery.flot.cust
// = require plugin/flot/jquery.flot.resize
// = require plugin/flot/jquery.flot.time
// = require plugin/flot/jquery.flot.tooltip.min
// = require plugin/flot/jquery.flot.fillbetween
// = require plugin/flot/jquery.flot.orderBar.min
// = require plugin/flot/jquery.flot.pie

// Vector Maps Plugin: Vectormap engine, Vectormap language
//= require plugin/vectormap/jquery-jvectormap-1.2.2.min
//= require plugin/vectormap/jquery-jvectormap-world-mill-en

// Full Calendar
//= require plugin/moment/moment
//= require plugin/chartjs/chart
//= require plugin/fullcalendar/jquery.fullcalendar

/* Forms */
//= require iCheck/icheck.min.js
//= require staps/jquery.steps.min.js
//= require validate/jquery.validate.min.js
//= require dropzone/dropzone.js
//= require summernote/summernote.min.js
//= require colorpicker/bootstrap-colorpicker.min.js
//= require cropper/cropper.min.js
//= require datapicker/bootstrap-datepicker.js
//= require ionRangeSlider/ion.rangeSlider.min.js
//= require jasny/jasny-bootstrap.min.js
//= require jsKnob/jquery.knob.js
//= require nouslider/jquery.nouislider.min.js
//= require switchery/switchery.js
//= require chosen/chosen.jquery.js
//= require fullcalendar/moment.min.js
//= require clockpicker/clockpicker.js
//= require daterangepicker/daterangepicker.js
//= require toastr/toastr.min.js

/*Sweet Alert*/
//= require sweetalert/sweetalert.min.js

$("nav li.active a").click(function(){
    console.log("hola")

    if ($(this).children("ul.nav-second-level.nav")){
        console.log("tiene")
    }else{
        console.log("no tiene")
    }

});