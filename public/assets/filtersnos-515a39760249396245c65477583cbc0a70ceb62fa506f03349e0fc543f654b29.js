let checkAll = document.getElementById('select_all');
//----Seleccionar todos los filtros para agregar o eliminar
checkAll.addEventListener('click', function(){
    let checkeado = document.getElementById('check_all').checked;

    //----Se agregan todo los filtros
    if( !checkeado ){

        swal({
                title: `¿Estas Seguro?`,
                text: `Agregara todos los filtros a una lista negra`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Continuar`,
                cancelButtonText: `Cancelar`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `Confirmar`,
                            text: `Agregar todos filtros`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `Continuar`,
                            cancelButtonText: `Cancelar`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Agregando todos los filtros
                                $.ajax({
                                    type: "POST",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: '../filtersnos/consultaFiltros',
                                    data: { agregar_todos: true },
                                    success: function(data){

                                        //--Esto agregar atributo id a cada checkbox - El id es de la tabla Filtersnos
                                        checkboxes = document.getElementsByClassName('addList');
                                        for (let i = 0; i < checkboxes.length; i++){
                                            let Filtro = checkboxes[i],
                                                idFiltro = Filtro.value;

                                            for( let i = 0; i < data.length; i++ ){
                                                let Filtrosno = data[i].IdFiltro;

                                                if ( idFiltro == Filtrosno ){

                                                    Filtro.setAttribute("id",`${data[i].id}`)

                                                }

                                            }

                                        }

                                        $('input.addList').prop('checked', true);

                                        document.getElementById('txt_todos').innerHTML = "Eliminar Todos";

                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `Se agregaron: ${data.length} filtros`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });

                                    }
                                });

                            }
                            else{

                                document.getElementById('select_all').classList.remove('active')
                                $('#check_all').prop('checked', false);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `No se agrego ningun filtro`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {
                    document.getElementById('select_all').classList.remove('active')
                    $('#check_all').prop('checked', false);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `No se agrego ningun filtro`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });


    //---Eliminar todos los filtros
    } else {
        swal({
                title: `¿Estas Seguro?`,
                text: `Eliminaras todos los filtros de la lista negra`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Continuar`,
                cancelButtonText: `Cancelar`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `Confirmar`,
                            text: `Eliminar todos filtros`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `Continuar`,
                            cancelButtonText: `Cancelar`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Eliminar todos los filtros
                                $.ajax({
                                    type: "POST",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: '../filtersnos/consultaFiltros',
                                    data: { eliminar_todos: true },
                                    success: function(data){

                                        $('input.addList').prop('checked', false).removeAttr("id");

                                        document.getElementById('txt_todos').innerHTML = "Agregar Todos";

                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `Se eliminaron: ${data.length} filtros`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });

                                    }
                                });

                            }
                            else{
                                document.getElementById('select_all').classList.add('active')
                                $('#check_all').prop('checked', true);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `No se elimino ningun filtro`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {
                    document.getElementById('select_all').classList.add('active')
                    $('#check_all').prop('checked', true);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `No se elimino ningun filtro`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });
    }

});

//----Mostrar los filtros
$(document).on('click', '.filters', function(){

    let popover = $(this).siblings('div.popover-custom');
        clase_none = popover.hasClass('d-none'),
        divAbierto = $('div.active');

    if ( clase_none ){
        //Si otro div esta abierto lo cierra
        divAbierto.removeClass('active');
        divAbierto.addClass('d-none');

        //El elemento al que le di clic
        popover.removeClass('d-none');
        popover.addClass('active');

    } else {
        popover.addClass('d-none');
        popover.removeClass('active');
    }
});


//----checkear un filtro
$(document).on('click', '.addList', function(e){
    let checked = this.checked,
        id_fil = this.value,
        inputCheck = $(this),
        id = this.id;

    //----Agregar a lista negra
    if ( checked ){
        swal({
                title: `¿Estas Seguro?`,
                text: `Agregara este filtro a lista negra`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Continuar`,
                cancelButtonText: `Cancelar`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `Confirmar`,
                            text: `Agregar a lista negra`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `Continuar`,
                            cancelButtonText: `Cancelar`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Agregando filtro
                                $.ajax({
                                    type: "POST",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: '../filtersnos',
                                    contentType: "application/json",
                                    dataType: "json",
                                    data: JSON.stringify({
                                        "_method": "create",
                                        IdFiltro: id_fil

                                    }),
                                    success: function(data){

                                        //Se agrega el id que corresponde como registro en la tabla
                                        $(`.check${id_fil}`).attr("id", `${data["id"]}`);

                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `Agregado con éxito`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });

                                    }
                                });

                            }
                            else{

                                //Esto es para que no se chechee el input
                                inputCheck.prop('checked', false);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `No se agrego ningun filtro`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {

                    //Esto es para que no se chechee el input
                    inputCheck.prop('checked', false);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `No se agrego ningun filtro`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });

    } else {
        // alert('no Checkeado')
        swal({
                title: `¿Estas Seguro?`,
                text: `Eliminaras de la lista negra este filtro`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Continuar`,
                cancelButtonText: `Cancelar`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `Confirmar`,
                            text: `Eliminaras de lista negra`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: `Continuar`,
                            cancelButtonText: `Cancelar`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                //---Eliminar un filtro de lista negra
                                $.ajax({
                                    type: "POST",
                                    beforeSend: function (xhr) {
                                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                                    },
                                    url: `../filtersnos/${id}`,
                                    data: {"_method": "delete"},
                                    success: function(data){

                                        inputCheck.removeAttr('id');

                                        swal({
                                            position: 'top-end',
                                            type: 'success',
                                            title: 'Ok',
                                            text: `Eliminado con éxito`,
                                            showConfirmButton: true,
                                            timer: 3000
                                        });

                                    }
                                });

                            }
                            else{

                                //Esto es para que no se deschechee el input
                                inputCheck.prop('checked', true);

                                swal({
                                    position: 'top-end',
                                    type: 'error',
                                    title: 'Ok',
                                    text: `No se agrego ningun filtro`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                            }
                        });

                } else {
                    //Esto es para que no se deschechee el input
                    inputCheck.prop('checked', true);

                    swal({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ok',
                        text: `No se agrego ningun filtro`,
                        showConfirmButton: true,
                        timer: 3000
                    });
                }
            });
    }

});
